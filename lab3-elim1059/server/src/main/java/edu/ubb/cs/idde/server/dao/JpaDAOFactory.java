package edu.ubb.cs.idde.server.dao;

public class JpaDAOFactory extends DAOFactory{

	public SongDAO getSongDAO() {
		return new JpaSongDAO();
	}

	public GenericDAO getGenericDAO() {
		return new JpaSongDAO();
	}

}
