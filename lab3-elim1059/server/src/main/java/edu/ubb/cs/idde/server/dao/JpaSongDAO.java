package edu.ubb.cs.idde.server.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.ubb.cs.idde.server.conn.HibernateUtil;
import edu.ubb.cs.idde.server.model.Song;

public class JpaSongDAO implements SongDAO, GenericDAO {

	private Session session;
	private Logger logger;

	public ArrayList<Song> getAllSongs() {
		logger = LoggerFactory.getLogger(JpaSongDAO.class);

		ArrayList<Song> songList = new ArrayList<Song>();
		session = HibernateUtil.getSession();
		logger.info("Connecting to database...");

		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			List songs = session.createQuery("FROM Song").list();

			for (Object o : songs) {
				Song s = (Song) o;
				int id = s.getId();
				String title  = s.getTitle();
				String artist = s.getArtist();
				String album = s.getAlbum();
				int year = s.getYear();

				songList.add(new Song(id, title, artist, album, year));
			}

			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			logger.debug(e.toString());
			e.printStackTrace();
		} finally {
			session.close();
			logger.info("Close connection...");
		}

		return songList;
	}

	public <T> ArrayList<T> getAll() {
		return (ArrayList<T>) getAllSongs();
	}
}
