package edu.ubb.cs.idde.server.conn;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory.getLogger(HibernateUtil.class);
	static {
		try {
			sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			logger.debug(ex.toString());
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static Session getSession() throws HibernateException {
		return sessionFactory.openSession();
	}
}

