package edu.ubb.cs.idde.server.dao;

import java.util.ArrayList;

public interface GenericDAO {

    public <T> ArrayList<T> getAll();

}
