package edu.ubb.cs.idde.server.dao;

public abstract class DAOFactory {
    
	public static DAOFactory getInstance(){ return new JpaDAOFactory(); }
	
	public abstract SongDAO getSongDAO();
	
	public abstract GenericDAO getGenericDAO();
}



