package edu.ubb.cs.idde.server.dao;

import java.util.ArrayList;

import edu.ubb.cs.idde.server.model.Song;

public interface SongDAO {

    public ArrayList<Song> getAllSongs();

}
