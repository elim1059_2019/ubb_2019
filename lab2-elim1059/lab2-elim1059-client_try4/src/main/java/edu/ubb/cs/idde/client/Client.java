package edu.ubb.cs.idde.client;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import edu.ubb.cs.idde.server.DAOFactory;
import edu.ubb.cs.idde.server.GenericDAO;

public class Client extends JFrame{

	private JButton button;
	private JTable table;
	private JComboBox<String> cb;
	private DefaultTableModel defaultTable;
	private ResourceBundle rb;
	String[] cs;
	private ArrayList<Object> pl = null;
	private Field[] f;

	public void uploadTable(){
		Class c = pl.get(0).getClass();
		f =  c.getDeclaredFields();
		
		defaultTable = new DefaultTableModel(pl.size()+1, f.length);
		table.setModel(defaultTable);
		for (int i = 0; i < f.length;  i++){
			defaultTable.setValueAt(rb.getString(f[i].getName()), 0, i);
			f[i].setAccessible(true);
		}
		
		for (int i = 1; i < pl.size()+1;  i++){
			for (int j = 0; j < f.length; j++){
				try {
					
					defaultTable.setValueAt(f[j].get(pl.get(i-1)), i, j);
					
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	public void uploadHeader(){
		for (int i = 0; i < f.length;  i++){
			defaultTable.setValueAt(rb.getString(f[i].getName()), 0, i);
		}
	}
	
	public Client(){
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				System.exit(0);
			}
		});
		
		setLayout(new BorderLayout());
		rb = ResourceBundle.getBundle("Bundle");	
		button = new JButton(rb.getString("button"));
		table = new JTable();
		cs = new String[] {"en", "hu", "de"};
		cb = new JComboBox<String>(cs);

		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					GenericDAO ud = DAOFactory.getInstance().getGenericDAO();
					pl = ud.getAll(); 
					
					uploadTable();
				} catch (Exception e1) {
					e1.printStackTrace();
				}

			}
		});	
		
		cb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				rb = ResourceBundle.getBundle("Bundle", new Locale(cb.getSelectedItem().toString(), cb.getSelectedItem().toString().toUpperCase()));
				button.setText(rb.getString("button"));
				
				if (pl != null){
					uploadHeader();
				}
			}
		});
		
		table.setSize(500, 300);
		table.setLocation(0,100);
		table.setVisible(true);
		table.setEnabled(false);

		add(cb,"North");
		add(button,"South");
		add(table,"Center");
	}

	public static void main(String[] args) {
		Client c = new Client();
		c.setBounds(700, 300, 700, 300);
		c.setVisible(true);
	}
}


