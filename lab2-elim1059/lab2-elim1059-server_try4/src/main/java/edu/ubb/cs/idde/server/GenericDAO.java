package edu.ubb.cs.idde.server;

import java.util.ArrayList;

public interface GenericDAO<T> {

    public ArrayList<T> getAll();

}
