package edu.ubb.cs.idde.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public final class ConnectionManager {

	private String databaseDriver;
	private String user;
	private String password;
	private String connectionString;
	

	
	public ConnectionManager(String databaseDriver, String connectionString, String user, String password){
		
		
		this.databaseDriver = databaseDriver;
		this.connectionString = connectionString;
		this.user = user;
		this.password = password;
		
		//System.out.println(connectionString);
	}
	
	public Statement getStatement(){
		
		Connection con = null;
		Statement statement = null;
		
		try {
			
			Class.forName(databaseDriver);
			System.out.println("Connecting to database...");
			con = DriverManager.getConnection(connectionString, user, password);	
			
			statement = con.createStatement();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		
		}  catch (SQLException e) {
			e.printStackTrace();
		
		}
		
		return statement;
		
	}
	
}
