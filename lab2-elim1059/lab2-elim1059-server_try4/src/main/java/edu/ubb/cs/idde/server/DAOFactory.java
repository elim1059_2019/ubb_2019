package edu.ubb.cs.idde.server;

public abstract class DAOFactory {
    
    public static DAOFactory getInstance(){ return new JdbcDAOFactory(); }
    
	public abstract SongDAO getSongDAO();
	
	public abstract GenericDAO getGenericDAO();
}



