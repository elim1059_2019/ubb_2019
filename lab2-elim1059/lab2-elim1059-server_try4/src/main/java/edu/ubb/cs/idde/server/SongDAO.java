package edu.ubb.cs.idde.server;

import java.util.ArrayList;

public interface SongDAO extends GenericDAO<Song> {

	ArrayList<Song> getAll();

    // no need to overload

}
