package edu.ubb.cs.idde.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import edu.ubb.cs.idde.server.Song;
import edu.ubb.cs.idde.server.SongDAO;

public class JdbcSongDAO implements SongDAO {

	private final ConnectionManager dbcm;
	private Statement statement;
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/song?autoReconnect=true&useSSL=false";
	static final String USER = "root";
	static final String PASS = "simplePassword";
	
	public JdbcSongDAO(){
		dbcm = new ConnectionManager(JDBC_DRIVER, DB_URL, USER, PASS);
	}
	
	public ArrayList<Song> getAll(){ 
		
		ArrayList<Song> resultList = new ArrayList<Song>();

		ResultSet rs;

		try{

			statement = dbcm.getStatement();
			rs = statement.executeQuery("SELECT * FROM song");

			while(rs.next()){
				String title  = rs.getString("title");
				String artist = rs.getString("artist");
				String album = rs.getString("album");
				int year = rs.getInt("year");

				resultList.add(new Song(title, artist, album, year));
			}

		} catch (SQLException e){
			e.printStackTrace();
		}

		return resultList;  
	}
}
